var bodyParser = require("body-parser");
methodOverride=require("method-override")
express = require("express");
mongoose = require("mongoose");
var app = express();
//App config

mongoose.connect("mongodb://localhost/restful_blog_app");
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(methodOverride("_method"))
app.use(bodyParser.urlencoded({ extended: true }));

//mongo config/model
var blogschema = new mongoose.Schema(
    {
        title: String,
        image: String,
        body: String,
        created: { type: Date, Default: Date.now }
    });

var Blog = mongoose.model("Blog", blogschema);
// Blog.create({

//     title:"Test blog",
//     image:"https://images.unsplash.com/photo-1437075130536-230e17c888b5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ff573beba18e5bf45eb0cccaa2c862b3&auto=format&fit=crop&w=500&q=60",
//     body:"Hey , this is blog post"

// });

app.get("/", function (req, res) {
    res.redirect("/blogs");
});

app.get("/blogs", function (req, res) {
    //reterivng all bloggs
    Blog.find({}, function (err, blogs) {
        if (err) {
            console.log(err);
        }
        else {

            res.render("index", { blogs: blogs });
        }
    });

});

app.get("/blogs/new", function (req, res) {
    res.render("new");
})

app.post("/blogs", function (req, res) {
    //creating new blog
    Blog.create(req.body.blog, function (err, newBlog) {
        if (err) {
            res.render("new");
        }
        else {
            res.redirect("/blogs");
        }
    });

});
//Show routes

app.get("/blogs/:id", function (req, res) {
    Blog.findById(req.params.id, function (err, foundBlog) {
        if (err) {
            console.log("eytydw");
            res.redirect("/blogs")
        }

        else {

            res.render("show", { blog: foundBlog });
        }
    });

});

//Edit route
app.get("/blogs/:id/edit", function (req, res) 
{
Blog.findById(req.params.id,function(err,editBlogfound)
{
    if (err) {
        res.redirect("/blogs");
    }
    else {
        res.render("edit",{editBlogfound:editBlogfound});
    }
});
});

//update route

app.put("/blogs/:id",function(req,res)
{
    Blog.findByIdAndUpdate(req.params.id,req.body.blog,function(err,updatedblog)//here find existing data and update with new
{
if(err)
{
    res.redirect("/blogs");
}else
{
res.redirect("/blogs/"+req.params.id);//here redirecting to show page
}
});
   //res.send("updated routes");
});

//Delete route

app.delete("/blogs/:id",function(req,res)
{
Blog.findByIdAndRemove(req.params.id,function(err)
{
if(err)
{
    res.redirect("/blogs");
}
else{
    res.redirect("/blogs");
}
});
});

app.listen("3000", function () {
    console.log("This server is running...");
});

